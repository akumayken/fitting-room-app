package com.akudreams.fittingroom

data class CatalogItem(val id: String = "", val name: String = "", val price: Float = -1f)