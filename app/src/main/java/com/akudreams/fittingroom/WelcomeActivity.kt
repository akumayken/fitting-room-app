package com.akudreams.fittingroom

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.akudreams.fittingroom.product.startProductActivity
import kotlinx.android.synthetic.main.activity_main.*

class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        welcomeTextView.setOnClickListener { startProductActivity() }
    }
}
