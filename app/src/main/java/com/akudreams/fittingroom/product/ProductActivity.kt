package com.akudreams.fittingroom.product

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.akudreams.fittingroom.CatalogItem
import com.akudreams.fittingroom.R
import com.google.firebase.firestore.FirebaseFirestore
import durdinapps.rxfirebase2.RxFirestore
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo

class ProductActivity : AppCompatActivity() {

    private var disposables = CompositeDisposable()
    private lateinit var viewModel: ProductViewModel
    private val products = mutableListOf<CatalogItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.product_activity)

        viewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)

        RxFirestore.getCollection(
                FirebaseFirestore.getInstance().collection("catalogue"),
                CatalogItem::class.java)
                .subscribe {
                    Log.d("ProductActivity", it.toString())
                    products.clear()
                    products.addAll(it)
                }.addTo(disposables)
    }

    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }

}

fun Context.startProductActivity() {
    startActivity(Intent(this, ProductActivity::class.java))
}